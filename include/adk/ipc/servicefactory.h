/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
// TODO(@mpascual): Based on dbus-cpp APIs and example code

#ifndef INCLUDE_ADK_IPC_SERVICEFACTORY_H_
#define INCLUDE_ADK_IPC_SERVICEFACTORY_H_

#include <memory>

#include <core/dbus/stub.h>
#include "adk/ipc/skeleton.h"

namespace adk {
namespace ipc {
/**
 * @brief Announces a service implementing the specified interface, dispatching
 * requests to the specified implementation.
 * @tparam Interface The interface that we want to expose a service for.
 * @tparam ServiceSkeleton The actual backend that calls are dispatched to.
 * @tparam ConstructArgs Parameter bundle passed on to the implementation c'tor.
 * @param [in] args Parameter pack to be passed on the implementation's c'tor.
 */
template <typename Interface, typename ServiceSkeleton,
          typename... ConstructArgs>
inline static typename ServiceSkeleton::Ptr announce_service_on_bus(
    const ConstructArgs&... args) {
  static_assert(std::is_base_of<Skeleton<Interface>, ServiceSkeleton>::value,
                "Implementation is not a Skeleton");
  static_assert(std::is_base_of<Interface, ServiceSkeleton>::value,
                "Implementation type does not inherit from Interface type.");
  return typename ServiceSkeleton::Ptr(new ServiceSkeleton(args...));
}

/**
 * @brief Announces a service implementing the specified interface, dispatching
 * requests to the specified implementation.
 * @tparam Interface The interface that we want to expose a service for.
 * @tparam ServiceSkeleton The actual backend that calls are dispatched to.
 * @param [in] args Parameter pack to be passed on the implementation's c'tor.
 */
template <typename Interface, typename ServiceStubSkeleton,
          typename... ConstructArgs>
inline static typename ServiceStubSkeleton::Ptr
announce_service_on_bus_with_listener(const ConstructArgs&... args) {
  static_assert(
      std::is_base_of<Skeleton<Interface>, ServiceStubSkeleton>::value,
      "ServiceSkeleton is not a Skeleton");
  static_assert(std::is_base_of<Interface, ServiceStubSkeleton>::value,
                "ServiceSkeleton type does not inherit from Interface type.");
  static_assert(
      std::is_base_of<core::dbus::Stub<Interface>, ServiceStubSkeleton>::value,
      "Not a stub");
  return typename ServiceStubSkeleton::Ptr(new ServiceStubSkeleton(args...));
}

/**
 * @brief Resolves an interface on the bus and creates a proxy object for it.
 * @tparam Interface The interface to be resolved on the bus.
 * @tparam ServiceStub The stub or proxy wrapping access to the interface.
 * @param [in] args Parameter pack to be passed on the implementation's c'tor.
 */
template <typename Interface, typename ServiceStub, typename... ConstructArgs>
inline static typename ServiceStub::Ptr resolve_service_on_bus(
    const ConstructArgs&... args) {
  static_assert(
      std::is_base_of<core::dbus::Stub<Interface>, ServiceStub>::value,
      "Not a stub");
  return typename ServiceStub::Ptr(new ServiceStub(args...));
}
}  // namespace ipc
}  // namespace adk
#endif  // INCLUDE_ADK_IPC_SERVICEFACTORY_H_

