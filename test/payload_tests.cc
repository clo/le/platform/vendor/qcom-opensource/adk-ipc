/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 * *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * * Neither the name of The Linux Foundation nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */
#include <gtest/gtest.h>

#include "adk/ipc/service.h"
#include "adk/ipc/session-bus.h"
#include "adk/ipc/traits.h"

#include <map>
#include <string>
#include <tuple>
#include <vector>

#include <core/testing/cross_process_sync.h>
#include <core/testing/fork_and_run.h>

#include <core/dbus/object.h>
#include <core/dbus/service.h>
#include <core/dbus/types/struct.h>

namespace dbus = core::dbus;
namespace ipc = adk::ipc;

// TODO(@mpascual): Consider a comparison of Maps routine as an improvement
TEST(AdkSignalsArg2Payload, AdkMsgMapWithSingleKeyAndBasicTypeVariant) {
  core::testing::CrossProcessSync server_up;
  core::testing::CrossProcessSync client_ready;

  // Create a qsap Message signal containing a single pair map entry.
  // where the variant value is a basic type, e.g. uint32:
  //   arg0 = "emitter_namespace",
  //   arg1 = "status_namespace",
  //   arg2 = {{"payload1", 1}}
  std::map<std::string, core::dbus::types::Variant> map;
  auto expected_command_payload = 1;
  map["payload1"] = core::dbus::types::Variant::encode<std::uint32_t>(
      expected_command_payload);
  auto expected_dbsignal_payload =
      std::make_tuple("destination_namespace", "command_namespace", map);

  auto server = [this, expected_dbsignal_payload,
                 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    auto service = dbus::Service::add_service<ipc::Service>(bus);
    auto object = service->add_object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});

    server_up.try_signal_ready_for(std::chrono::milliseconds{500});

    EXPECT_EQ(std::uint32_t(1), client_ready.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    object->emit_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage,
        ipc::Service::Interfaces::Events1::Signals::AdkMessage::ArgumentType>(
        expected_dbsignal_payload);

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  auto client = [this, expected_dbsignal_payload, expected_command_payload,
                 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    std::thread t{std::bind(&dbus::Bus::run, bus)};

    EXPECT_EQ(std::uint32_t(1), server_up.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    auto stub_server = dbus::Service::use_service(
        bus, dbus::traits::Service<ipc::Service>::interface_name());
    auto object = stub_server->object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});
    auto objectsignal =
            object->get_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage>();

    ipc::Service::AdkMessageSignalPayload received_dbsignal_payload;

    objectsignal->connect([bus, &received_dbsignal_payload](
        ipc::Service::AdkMessageSignalPayload status) {
      received_dbsignal_payload = status;
      bus->stop();
    });

    client_ready.try_signal_ready_for(std::chrono::milliseconds{500});

    if (t.joinable()) t.join();

    // Once the variant is extracted its entry in the payload map becomes
    // invalid. Extract status payload to a local variable:
    auto received_command_payload = std::get<2>(received_dbsignal_payload)
                  .cbegin()->second.as<std::uint32_t>();
    std::cout << "Received Key-Value pair: "
        << (std::get<2>(received_dbsignal_payload)).cbegin()->first
        << ", " << received_command_payload << std::endl;

    EXPECT_EQ(std::get<0>(expected_dbsignal_payload),
              std::get<0>(received_dbsignal_payload));
    EXPECT_EQ(std::get<1>(expected_dbsignal_payload),
              std::get<1>(received_dbsignal_payload));
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload)).size(),
              (std::get<2>(received_dbsignal_payload)).size());
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload)).cbegin()->first,
              (std::get<2>(received_dbsignal_payload)).cbegin()->first);
    EXPECT_EQ(std::uint32_t(expected_command_payload),
              (received_command_payload));

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  EXPECT_EQ(core::testing::ForkAndRunResult::empty,
            core::testing::fork_and_run(server, client));
}

TEST(AdkSignalsArg2Payload, MultipleAdkMsgsWithMultipleListeners) {
  core::testing::CrossProcessSync server_up;
  core::testing::CrossProcessSync client_ready;

  // Create two independent qsap status signals to be emitted by the server
  std::map<std::string, core::dbus::types::Variant> map1;
  auto expected_status_payload_1 = 1;
  map1["payload1"] = core::dbus::types::Variant::encode<std::uint32_t>(
      expected_status_payload_1);
  auto expected_dbsignal_payload_1 =
      std::make_tuple("server", "map1", map1);

  std::map<std::string, core::dbus::types::Variant> map2;
  auto expected_status_payload_2 = 2;
  map2["payload2"] = core::dbus::types::Variant::encode<std::uint32_t>(
      expected_status_payload_2);
  auto expected_dbsignal_payload_2 =
      std::make_tuple("server", "map2", map2);

  auto server = [this, expected_dbsignal_payload_1,
                 expected_dbsignal_payload_2,
                 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    auto service = dbus::Service::add_service<ipc::Service>(bus);
    auto object = service->add_object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});

    server_up.try_signal_ready_for(std::chrono::milliseconds{500});

    EXPECT_EQ(std::uint32_t(1), client_ready.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    object->emit_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage,
                        ipc::Service::Interfaces::Events1::Signals::AdkMessage::ArgumentType>(
        expected_dbsignal_payload_1);

    object->emit_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage,
                        ipc::Service::Interfaces::Events1::Signals::AdkMessage::ArgumentType>(
        expected_dbsignal_payload_2);

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  auto client = [this, expected_dbsignal_payload_1, expected_status_payload_1,
 expected_dbsignal_payload_2, expected_status_payload_2,
 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    std::thread t{std::bind(&dbus::Bus::run, bus)};

    EXPECT_EQ(std::uint32_t(1), server_up.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    auto stub_server = dbus::Service::use_service(
        bus, dbus::traits::Service<ipc::Service>::interface_name());
    auto object = stub_server->object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});
    auto objectsignal =
         object->get_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage>();

    ipc::Service::AdkMessageSignalPayload received_dbsignal_payload_1;
    ipc::Service::AdkMessageSignalPayload received_dbsignal_payload_2;

    std::vector<std::pair<std::size_t, std::string>> match_args_1{{0, "server"},
                                                                  {1, "map1"}};
    objectsignal->connect_with_match_args([bus, &received_dbsignal_payload_1](
          ipc::Service::AdkMessageSignalPayload status) {
          std::cout << "AdkStatus map1 signal has been received \n";
          received_dbsignal_payload_1 = status;
          bus->stop();
        },
    match_args_1);

    std::vector<std::pair<std::size_t, std::string>> match_args_2{{0, "server"},
                                                                  {1, "map2"}};
    objectsignal->connect_with_match_args([bus, &received_dbsignal_payload_2](
          ipc::Service::AdkMessageSignalPayload status) {
          std::cout << "AdkStatus map2 signal has been received \n";
          received_dbsignal_payload_2 = status;
          bus->stop();
        },
    match_args_2);

    client_ready.try_signal_ready_for(std::chrono::milliseconds{500});

    if (t.joinable()) t.join();

    EXPECT_EQ(std::get<0>(expected_dbsignal_payload_1),
              std::get<0>(received_dbsignal_payload_1));
    EXPECT_EQ(std::get<1>(expected_dbsignal_payload_1),
              std::get<1>(received_dbsignal_payload_1));
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload_1)).size(),
              (std::get<2>(received_dbsignal_payload_1)).size());
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload_1)).cbegin()->first,
              (std::get<2>(received_dbsignal_payload_1)).cbegin()->first);
    EXPECT_EQ(std::uint32_t(expected_status_payload_1),
              (std::get<2>(received_dbsignal_payload_1))
                  .cbegin()->second.as<std::uint32_t>());

    EXPECT_EQ(std::get<0>(expected_dbsignal_payload_2),
              std::get<0>(received_dbsignal_payload_2));
    EXPECT_EQ(std::get<1>(expected_dbsignal_payload_2),
              std::get<1>(received_dbsignal_payload_2));
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload_2)).size(),
              (std::get<2>(received_dbsignal_payload_2)).size());
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload_2)).cbegin()->first,
              (std::get<2>(received_dbsignal_payload_2)).cbegin()->first);
    EXPECT_EQ(std::uint32_t(expected_status_payload_2),
              (std::get<2>(received_dbsignal_payload_2))
                  .cbegin()->second.as<std::uint32_t>());

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  EXPECT_EQ(core::testing::ForkAndRunResult::empty,
            core::testing::fork_and_run(server, client));
}

TEST(AdkSignalsArg2Payload, AdkMsgMapWithMultipleKeysAndBasicTypeVariants) {
  core::testing::CrossProcessSync server_up;
  core::testing::CrossProcessSync client_ready;

  // Create a qsap status signal containing two pairs in the payload map,
  // where the variants are of different basic types, e.g. uint32, string:
  //   arg0 = "emitter_namespace",
  //   arg1 = "status_namespace",
  //   arg2 = {{"payload1", 1},
  //          {"payload2", "long"}}
  std::map<std::string, core::dbus::types::Variant> map;
  map["payload1"] = core::dbus::types::Variant::encode<std::uint32_t>(1);
  map["payload2"] = core::dbus::types::Variant::encode<std::string>("long");
  // std::map<std::string, std::string> expected_map = {{"payload1", "id_1"},
  // {"payload2", "long"}};

  auto expected_dbsignal_payload =
      std::make_tuple("emitter_namespace", "status_namespace", map);

  auto server = [this, expected_dbsignal_payload,
 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    auto server = dbus::Service::add_service<ipc::Service>(bus);
    auto object = server->add_object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});

    server_up.try_signal_ready_for(std::chrono::milliseconds{500});

    EXPECT_EQ(std::uint32_t(1), client_ready.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    object->emit_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage,
                        ipc::Service::Interfaces::Events1::Signals::AdkMessage::ArgumentType>(
        expected_dbsignal_payload);

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  auto client = [this, expected_dbsignal_payload,
 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    std::thread t{std::bind(&dbus::Bus::run, bus)};

    EXPECT_EQ(std::uint32_t(1), server_up.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    auto stub_server = dbus::Service::use_service(
        bus, dbus::traits::Service<ipc::Service>::interface_name());
    auto object = stub_server->object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});
    auto objectsignal =
         object->get_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage>();

    ipc::Service::AdkMessageSignalPayload received_dbsignal_payload;

    objectsignal->connect([bus, &received_dbsignal_payload](
        ipc::Service::AdkMessageSignalPayload status) {
      received_dbsignal_payload = status;
      bus->stop();
    });

    client_ready.try_signal_ready_for(std::chrono::milliseconds{500});

    if (t.joinable()) t.join();

    EXPECT_EQ(std::get<0>(expected_dbsignal_payload),
              std::get<0>(received_dbsignal_payload));
    EXPECT_EQ(std::get<1>(expected_dbsignal_payload),
              std::get<1>(received_dbsignal_payload));
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload)).size(),
              (std::get<2>(received_dbsignal_payload)).size());
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload)).cbegin()->first,
              (std::get<2>(received_dbsignal_payload)).cbegin()->first);

    // Once the variant is extracted its entry in the payload map becomes
    // invalid. Extract status payload only once and compare to expected value,
    auto payload_map = std::get<2>(received_dbsignal_payload);
    EXPECT_EQ(std::uint32_t(1), payload_map.at("payload1").as<std::uint32_t>());
    EXPECT_EQ(std::string("long"),
              payload_map.at("payload2").as<std::string>());

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  EXPECT_EQ(core::testing::ForkAndRunResult::empty,
            core::testing::fork_and_run(server, client));
}

TEST(AdkSignalsArg2Payload, AdkMsgMapWithSingleKeyAndStructVariant) {
  core::testing::CrossProcessSync server_up;
  core::testing::CrossProcessSync client_ready;

  // Create a qsap status signal containing a single pair map entry.
  // where the variant value is a struct type, modeled as a tuple
  // containing various basic types:
  //   arg0="emitter_namespace",
  //   arg1="status_namespace",
  //   arg2={{"payload1", Struct}}
  std::map<std::string, core::dbus::types::Variant> map;

  typedef core::dbus::types::Struct<
      std::tuple<std::string, std::string, std::int32_t>>
      Struct;
  const Struct expected_status_payload =
      Struct{std::make_tuple("pattern,1", "group,2", 56)};

  map["start_flash_pattern"] =
      core::dbus::types::Variant::encode(expected_status_payload);

  // Construct status signal payload
  auto expected_dbsignal_payload =
      std::make_tuple("emitter_namespace", "status_namespace", map);

  auto server = [this, expected_dbsignal_payload,
                 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    auto server = dbus::Service::add_service<ipc::Service>(bus);
    auto object = server->add_object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});

    server_up.try_signal_ready_for(std::chrono::milliseconds{500});

    EXPECT_EQ(std::uint32_t(1), client_ready.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    object->emit_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage,
                        ipc::Service::Interfaces::Events1::Signals::AdkMessage::ArgumentType>(
        expected_dbsignal_payload);

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  auto client = [this, expected_dbsignal_payload, expected_status_payload,
                 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    std::thread t{std::bind(&dbus::Bus::run, bus)};

    EXPECT_EQ(std::uint32_t(1), server_up.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    auto stub_server = dbus::Service::use_service(
        bus, dbus::traits::Service<ipc::Service>::interface_name());
    auto object = stub_server->object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});
    auto objectsignal =
          object->get_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage>();

     ipc::Service::AdkMessageSignalPayload received_dbsignal_payload;
    Struct received_status_payload;

    objectsignal->connect([bus, &received_status_payload,
                           &received_dbsignal_payload](
                      ipc::Service::AdkMessageSignalPayload status) {
      received_dbsignal_payload = status;

      // Once the variant is extracted its entry in the payload map becomes
      // invalid. Extract status payload to a local variable here:
      received_status_payload = std::get<2>(status)
      .at("start_flash_pattern").as<Struct>();
      std::cout << "start_flash_pattern: "
                << std::get<0>(received_status_payload.value) << " "
                << std::get<1>(received_status_payload.value) << " "
                << std::get<2>(received_status_payload.value) << std::endl;
      bus->stop();
    });

    client_ready.try_signal_ready_for(std::chrono::milliseconds{2000});

    if (t.joinable()) t.join();

    EXPECT_EQ(std::get<0>(expected_dbsignal_payload),
              std::get<0>(received_dbsignal_payload));
    EXPECT_EQ(std::get<1>(expected_dbsignal_payload),
              std::get<1>(received_dbsignal_payload));
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload)).size(),
              (std::get<2>(received_dbsignal_payload)).size());
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload)).cbegin()->first,
              (std::get<2>(received_dbsignal_payload)).cbegin()->first);
    EXPECT_EQ(std::get<0>(expected_status_payload.value),
              std::get<0>(received_status_payload.value));  // "pattern,1"
    EXPECT_EQ(std::get<1>(expected_status_payload.value),
              std::get<1>(received_status_payload.value));  // "group,2"
    EXPECT_EQ(std::get<2>(expected_status_payload.value),
              std::get<2>(received_status_payload.value));  // 56

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  EXPECT_EQ(core::testing::ForkAndRunResult::empty,
            core::testing::fork_and_run(server, client));
}

TEST(AdkSignalsArg2Payload,
     AdkMsgMapWithMultipleKeysAndMixedTypeVariantsWithTwoListeners) {
  core::testing::CrossProcessSync server_up;
  core::testing::CrossProcessSync client_ready;

  // Create an status signal to be emitted by the server:
  // based on the following ScanInfo structure:
  //    std::string ssid;
  //    uint8_t bssid[6];
  //    int rssi;
  //    uint16_t caps;
  //    int wpa;

  //   arg0 = "Connectivity",
  //   arg1 = "wifi",
  //   arg2 = {["ssid"] = "QGuest",
  //           ["bssid"] = {0x00, 0xF2, 0x8B, 0x2B, 0xEB, 0xAB},
  //           ["rssi"] = 2,
  //           ["caps"] = 1,
  //           ["wpa"] = 0}

  std::map<std::string, core::dbus::types::Variant> map;

  typedef std::tuple<std::int8_t, std::int8_t, std::int8_t, std::int8_t,
                     std::int8_t, std::int8_t> Bssid;
  typedef core::dbus::types::Struct<Bssid> Struct;
  const Struct expected_bssid =
      Struct{std::make_tuple(0x00, 0xF2, 0x8B, 0x2B, 0xEB, 0xAB)};

  map["ssid"] = core::dbus::types::Variant::encode<std::string>("QGuest");
  map["bssid"] = core::dbus::types::Variant::encode(expected_bssid);
  map["rssi"] = core::dbus::types::Variant::encode<std::int32_t>(16);
  map["caps"] = core::dbus::types::Variant::encode<std::uint16_t>(16);
  map["wpa"] = core::dbus::types::Variant::encode<std::int16_t>(0);
  auto expected_dbsignal_payload = std::make_tuple("Connectivity", "wifi", map);

  auto server = [this, expected_dbsignal_payload,
                 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    auto server = dbus::Service::add_service<ipc::Service>(bus);
    auto object = server->add_object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});

    server_up.try_signal_ready_for(std::chrono::milliseconds{500});

    EXPECT_EQ(std::uint32_t(1), client_ready.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    object->emit_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage,
                        ipc::Service::Interfaces::Events1::Signals::AdkMessage::ArgumentType>(
        expected_dbsignal_payload);

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  auto client = [this, expected_dbsignal_payload, expected_bssid,
                 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    std::thread t{std::bind(&dbus::Bus::run, bus)};

    EXPECT_EQ(std::uint32_t(1), server_up.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    auto stub_server = dbus::Service::use_service(
        bus, dbus::traits::Service<ipc::Service>::interface_name());
    auto object = stub_server->object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});
    auto objectsignal =
        object->get_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage>();

    ipc::Service::AdkMessageSignalPayload received_dbsignal_payload;
    objectsignal->connect([bus, &received_dbsignal_payload](
        ipc::Service::AdkMessageSignalPayload status) {
      received_dbsignal_payload = status;
      bus->stop();
    });

    ipc::Service::AdkMessageSignalPayload received_dbsignal_payload2;
    objectsignal->connect([bus, &received_dbsignal_payload2](
        ipc::Service::AdkMessageSignalPayload status) {
      received_dbsignal_payload2 = status;
      bus->stop();
    });

    client_ready.try_signal_ready_for(std::chrono::milliseconds{500});

    if (t.joinable()) t.join();

    EXPECT_EQ(std::get<0>(expected_dbsignal_payload),
              std::get<0>(received_dbsignal_payload));
    EXPECT_EQ(std::get<1>(expected_dbsignal_payload),
              std::get<1>(received_dbsignal_payload));
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload)).size(),
              (std::get<2>(received_dbsignal_payload)).size());

    auto payload_map = std::get<2>(received_dbsignal_payload);
    EXPECT_EQ(payload_map.at("ssid").as<std::string>(),
              std::string("QGuest"));
    EXPECT_EQ(payload_map.at("bssid").as<Struct>(), expected_bssid);
    EXPECT_EQ(payload_map.at("rssi").as<std::int32_t>(),
              std::int32_t(16));
    EXPECT_EQ(payload_map.at("caps").as<std::uint16_t>(),
              std::uint16_t(16));
    EXPECT_EQ(payload_map.at("wpa").as<std::int16_t>(),
              std::int16_t(0));

    EXPECT_EQ(std::get<0>(expected_dbsignal_payload),
              std::get<0>(received_dbsignal_payload2));
    EXPECT_EQ(std::get<1>(expected_dbsignal_payload),
              std::get<1>(received_dbsignal_payload2));
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload)).size(),
              (std::get<2>(received_dbsignal_payload2)).size());

    auto payload_map2 = std::get<2>(received_dbsignal_payload2);
    EXPECT_EQ(payload_map2.at("ssid").as<std::string>(),
              std::string("QGuest"));
    EXPECT_EQ(payload_map2.at("bssid").as<Struct>(), expected_bssid);
    EXPECT_EQ(payload_map2.at("rssi").as<std::int32_t>(),
              std::int32_t(16));
    EXPECT_EQ(payload_map2.at("caps").as<std::uint16_t>(),
              std::uint16_t(16));
    EXPECT_EQ(payload_map2.at("wpa").as<std::int16_t>(),
              std::int16_t(0));

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  EXPECT_EQ(core::testing::ForkAndRunResult::empty,
            core::testing::fork_and_run(server, client));
}

TEST(AdkSignalsArg2Payload,  AdkMsgMapWithMultipleKeysAndMapVariants) {
  core::testing::CrossProcessSync server_up;
  core::testing::CrossProcessSync client_ready;

  // Create an status signal to be emitted by the server:
  // based on the followint struct ScanInfo structure:
  //    std::string ssid;
  //    uint8_t bssid[6];
  //    int rssi;
  //    uint16_t caps;
  //    int wpa;

  //   arg0 = "Connectivity",
  //   arg1 = "wifi",
  //   arg2 = {["scaninfo1"], {["ssid"] = "QGuest",
  //                          ["bssid"] = "00:F2:8B:2B:EB:A6"
  //                          ["rssi"] = 2,
  //                          ["caps"] = 1,
  //                          ["encription"] = {["wep"] = "false",
  //                                            ["wpa"] = "3"}
  //         }
  //         {["scaninfo2"],  {["ssid"] = "QGuest",
  //                           ["bssid"] = "00:F2:8B:2B:EB:A6"
  //                           ["rssi"] = 2,
  //                           ["caps"] = 1,
  //                           ["encription"] = {["wep"] = "false",
  //                                             ["wpa"] = "3"}
  //         }

  std::map<std::string, core::dbus::types::Variant> map;

  typedef std::map<std::string, core::dbus::types::Variant> ScanInfoMapEntry;
  typedef std::map<std::string, std::string> EncriptionInfo;

  const EncriptionInfo encription{{"wep", "false"}, {"wpa", "3"}};
  const ScanInfoMapEntry scaninfomapentry1{
  {"ssid", core::dbus::types::Variant::encode<std::string>("QGuest")},
  {"bssid",
    core::dbus::types::Variant::encode<std::string>("00:F2:8B:2B:EB:A6")},
  {"rssi", core::dbus::types::Variant::encode<std::int32_t>(16)},
  {"caps", core::dbus::types::Variant::encode<std::uint16_t>(16)},
  {"encription", core::dbus::types::Variant::encode(encription)}};
  const ScanInfoMapEntry scaninfomapentry2{
  {"ssid", core::dbus::types::Variant::encode<std::string>("QGuest")},
  {"bssid",
    core::dbus::types::Variant::encode<std::string>("00:F2:8B:2B:EB:A6")},
  {"rssi", core::dbus::types::Variant::encode<std::int32_t>(16)},
  {"caps", core::dbus::types::Variant::encode<std::uint16_t>(16)},
  {"encription", core::dbus::types::Variant::encode(encription)}};

  map["scaninfo1"] = core::dbus::types::Variant::encode(scaninfomapentry1);
  map["scaninfo2"] = core::dbus::types::Variant::encode(scaninfomapentry2);
  auto expected_dbsignal_payload = std::make_tuple("Connectivity", "wifi", map);

  auto server = [this, expected_dbsignal_payload,
                 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    auto server = dbus::Service::add_service<ipc::Service>(bus);
    auto object = server->add_object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});

    server_up.try_signal_ready_for(std::chrono::milliseconds{500});

    EXPECT_EQ(std::uint32_t(1), client_ready.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    object->emit_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage,
                        ipc::Service::Interfaces::Events1::Signals::AdkMessage::ArgumentType>(
        expected_dbsignal_payload);

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  auto client = [this, expected_dbsignal_payload,
                 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    std::thread t{std::bind(&dbus::Bus::run, bus)};

    EXPECT_EQ(std::uint32_t(1), server_up.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    auto stub_server = dbus::Service::use_service(
        bus, dbus::traits::Service<ipc::Service>::interface_name());
    auto object = stub_server->object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});
    auto objectsignal =
        object->get_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage>();

    ipc::Service::AdkMessageSignalPayload received_dbsignal_payload;

    objectsignal->connect([bus, &received_dbsignal_payload](
        ipc::Service::AdkMessageSignalPayload status) {
      received_dbsignal_payload = status;
      bus->stop();
    });

    client_ready.try_signal_ready_for(std::chrono::milliseconds{500});

    if (t.joinable()) t.join();

    EXPECT_EQ(std::get<0>(expected_dbsignal_payload),
              std::get<0>(received_dbsignal_payload));
    EXPECT_EQ(std::get<1>(expected_dbsignal_payload),
              std::get<1>(received_dbsignal_payload));
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload)).size(),
              (std::get<2>(received_dbsignal_payload)).size());

    auto payload_map = std::get<2>(received_dbsignal_payload);

    auto scaninfo1_map = payload_map.at("scaninfo1").as<ScanInfoMapEntry>();
    EXPECT_EQ(scaninfo1_map.at("ssid").as<std::string>(),
              std::string("QGuest"));
    EXPECT_EQ(scaninfo1_map.at("bssid").as<std::string>(),
              std::string("00:F2:8B:2B:EB:A6"));
    EXPECT_EQ(scaninfo1_map.at("rssi").as<std::int32_t>(),
              std::int32_t(16));
    EXPECT_EQ(scaninfo1_map.at("caps").as<std::uint16_t>(),
              std::uint16_t(16));
    auto encription1 = scaninfo1_map.at("encription").as<EncriptionInfo>();
    EXPECT_EQ(encription1.at("wep"), std::string("false"));
    EXPECT_EQ(encription1.at("wpa"), std::string("3"));

    auto scaninfo2_map = payload_map.at("scaninfo2").as<ScanInfoMapEntry>();
    EXPECT_EQ(scaninfo2_map.at("ssid").as<std::string>(),
              std::string("QGuest"));
    EXPECT_EQ(scaninfo2_map.at("bssid").as<std::string>(),
              std::string("00:F2:8B:2B:EB:A6"));
    EXPECT_EQ(scaninfo2_map.at("rssi").as<std::int32_t>(),
              std::int32_t(16));
    EXPECT_EQ(scaninfo2_map.at("caps").as<std::uint16_t>(),
              std::uint16_t(16));
    auto encription2 = scaninfo2_map.at("encription").as<EncriptionInfo>();
    EXPECT_EQ(encription2.at("wep"), std::string("false"));
    EXPECT_EQ(encription2.at("wpa"), std::string("3"));

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  EXPECT_EQ(core::testing::ForkAndRunResult::empty,
            core::testing::fork_and_run(server, client));
}


TEST(AdkSignalsArg2Payload,
AdkMsgMapWithMultipleKeysWithVectorAndMapAndTupleVariants) {
  core::testing::CrossProcessSync server_up;
  core::testing::CrossProcessSync client_ready;

  // Create a qsap signal containing a std::vector entry.
  std::map<std::string, core::dbus::types::Variant> map;

  typedef core::dbus::types::Struct<std::tuple<double, double>> Struct;
  auto item1 = Struct{std::make_tuple(1.4, 1.5)};
  auto item2 = Struct{std::make_tuple(1.6, 1.7)};

  typedef std::vector<Struct> Vector;
  const Vector vctr{item1, item2};
  map["vectorentry"] = core::dbus::types::Variant::encode(vctr);

  typedef std::map<std::string, std::string> Map;
  const Map mp{{"wep", "false"}, {"wpa", "0"}};
  map["mapentry"] = core::dbus::types::Variant::encode(mp);

  typedef std::tuple<double, double> Tuple;
  const Tuple tpl{1.6, 1.7};
  // map["tupleentry"] = core::dbus::types::Variant::encode(tpl);
  // Note: tuples are supported embeded in a Struct
  typedef core::dbus::types::Struct<Tuple> Struct;
  const Struct s_tpl = Struct{tpl};
  map["s_tupleentry"] = core::dbus::types::Variant::encode(s_tpl);

  auto expected_dbsignal_payload = std::make_tuple("Connectivity", "wifi", map);

  auto server = [this, expected_dbsignal_payload,
 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    auto server = dbus::Service::add_service<ipc::Service>(bus);
    auto object = server->add_object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});

    server_up.try_signal_ready_for(std::chrono::milliseconds{500});

    EXPECT_EQ(std::uint32_t(1), client_ready.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    object->emit_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage,
                        ipc::Service::Interfaces::Events1::Signals::AdkMessage::ArgumentType>(
        expected_dbsignal_payload);

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  auto client = [this, expected_dbsignal_payload,
 &server_up, &client_ready]() {
    auto bus = ipc::the_session_bus();
    std::thread t{std::bind(&dbus::Bus::run, bus)};

    EXPECT_EQ(std::uint32_t(1), server_up.wait_for_signal_ready_for(
                                    std::chrono::milliseconds{500}));

    auto stub_server = dbus::Service::use_service(
        bus, dbus::traits::Service<ipc::Service>::interface_name());
    auto object = stub_server->object_for_path(dbus::types::ObjectPath{
        dbus::traits::Service<ipc::Service>::object_path()});
    auto objectsignal =
        object->get_signal<ipc::Service::Interfaces::Events1::Signals::AdkMessage>();

    ipc::Service::AdkMessageSignalPayload received_dbsignal_payload;

    objectsignal->connect([bus, &received_dbsignal_payload](
        ipc::Service::AdkMessageSignalPayload status) {
      received_dbsignal_payload = status;
      bus->stop();
    });

    client_ready.try_signal_ready_for(std::chrono::milliseconds{500});

    if (t.joinable()) t.join();

    EXPECT_EQ(std::get<0>(expected_dbsignal_payload),
              std::get<0>(received_dbsignal_payload));
    EXPECT_EQ(std::get<1>(expected_dbsignal_payload),
              std::get<1>(received_dbsignal_payload));
    EXPECT_EQ((std::get<2>(expected_dbsignal_payload)).size(),
              (std::get<2>(received_dbsignal_payload)).size());

    return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure
                                         : core::posix::exit::Status::success;
  };

  EXPECT_EQ(core::testing::ForkAndRunResult::empty,
            core::testing::fork_and_run(server, client));
}


int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}
