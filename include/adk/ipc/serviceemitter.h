/*
Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef INCLUDE_ADK_IPC_SERVICEEMITTER_H_
#define INCLUDE_ADK_IPC_SERVICEEMITTER_H_

#include <memory>
#include <string>

#include <core/dbus/object.h>
#include "adk/ipc/service.h"
#include "adk/ipc/skeleton.h"
#include "adk/ipc/traits.h"

namespace adk {
namespace ipc {
/**
 * @brief Emitter is an abstract specialized class of the dbus-cpp Skeleton
 * template class to expose the com.qualcomm.qti.adk.Events1 interface of the
 * "com.qualcomm.qti.adk" service on D-Bus implemented by the
 * /com/qualcomm/qti/adk1 object.
 * It hides the dbus-cpp layer from its derived classes, allowing connection
 * to the session bus and emission of Message signals on the bus.
 */
class Emitter : public Skeleton<Service> {
 public:
  ~Emitter() override = default;

  /**
    * @brief LaunchConnection should be called after the specialized derived
    * service instantiation to properly initialize the connection to the ADK IPC
    * bus. It will explicitly request a dbus connection name for this module and
    * it will generate a call to the derived service implementation of
    * StartServerService().
    * @return false if an error condition prevents the service from being
    * successfully launched, true otherwise.
    */
  virtual bool LaunchConnection();

  /**
    * @brief Emits Message Signal with the given payload.
    * @param payload The arguments to this signal
    * @return false if an error condition prevents this signal from being
    * emited, true otherwise.
    */
  virtual bool EmitMessageSignal(const AdkMessageSignalPayload& payload) const;

 protected:
  /**
   * @brief Constructs an instance of Emitter and connects to the
   * bus with the specified bus/connection name.
   * @param conname The bus/connection name used by this module.
   */
  explicit Emitter(const std::string& conname);

  /**
   * @brief Constructs an instance of Emitter and connects to the
   * bus with the specified bus/connection name.
   * @param bus The instance of the bus this module is connecting to.
   * @param conname The bus/connection name used by this module.
   */
  Emitter(const core::dbus::Bus::Ptr& bus, const std::string& conname);

  // Disabling copy and assignment operators
  Emitter(const Emitter&) = delete;
  Emitter& operator=(const Emitter&) = delete;

  /**
    * @brief StartServerService must be implemented by derived classes.
    * It will be called after dbus initialization has been completed to allow
    * derived services to implement their internal processing.
    * The specialized services can then emit signals by using the
    * EmitMessageSignal() public API methods.
    * @return false if an error condition prevents this service from being
    * ready, true otherwise.
    */
  virtual bool StartServerService() = 0;

 private:
  core::dbus::Object::Ptr object_;
  const std::string connectionname_;
};
}  // namespace ipc
}  // namespace adk

#endif  // INCLUDE_ADK_IPC_SERVICEEMITTER_H_
