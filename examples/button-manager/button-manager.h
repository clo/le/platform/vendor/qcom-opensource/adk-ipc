/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef EXAMPLES_BUTTON_MANAGER_BUTTON_MANAGER_H_
#define EXAMPLES_BUTTON_MANAGER_BUTTON_MANAGER_H_

#include <memory>

#include "adk/ipc/serviceemitter.h"

namespace adk {
namespace examples {
/**
 * @brief This is a simple example of a adk-ipc service server that emits
 * ADK signals. It is implemented following the three steps below:
 *
 * Step1: API implementation
 * It implements a derived class from the base adk-ipc Emitter
 * class, passing its module's name to its base class constructor to be used
 * as this service's D-BUS connection/bus name and providing a Ptr typedef to
 * its own shared_ptr.
 * It implements the StartServerService API where this server's main loop and
 * internal processing is initialized. The service emits ADK signals by
 * calling the public adk-ipc API methods EmitMessageSignal or
 * EmitStatusSignal
 *
 * Step2: Private Detail implementation
 * It implements its internal processing privately. This simple example service
 * server just runs in a loop printing and emitting signals every 'n' seconds.
 *
 * Step3: Public instantiation and deployment
 * This service is instantiated from the main() application by calling the
 * adk-ipc server announcer utility announce_service_on_bus().
 * The ADK IPC service for this client is then started by calling the public
 * API LaunchAdkConnection();
 *
 * */
class ButtonMgrService final : public ipc::Emitter {
 public:
  // Needed by announce_service_on_bus()
  typedef std::shared_ptr<ButtonMgrService> Ptr;

 public:
  inline ButtonMgrService()
      : ipc::Emitter("com.qualcomm.qti.adk1.button-manager.example") {}

  void StopServerService();

 private:
  // From Emitter
  bool StartServerService() override;

 private:
  bool shutting_down_ = {false};
};
}  // namespace examples
}  // namespace adk

#endif  // EXAMPLES_BUTTON_MANAGER_BUTTON_MANAGER_H_
