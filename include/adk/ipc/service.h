/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef INCLUDE_ADK_IPC_SERVICE_H_
#define INCLUDE_ADK_IPC_SERVICE_H_

#include <map>
#include <string>
#include <tuple>

#include <core/dbus/types/variant.h>

namespace adk {
namespace ipc {
/**
  * @brief Service contains the definition of the DBUS ADK service
  * interface.
  */
struct Service {
  typedef core::dbus::types::Variant AdkVariant;

  struct Interfaces {
    struct Events1 {
      static const std::string& name() {
        static const std::string s{"com.qualcomm.qti.adk.Events1"};
        return s;
      }

      struct Signals {
        struct AdkMessage {
          inline static std::string name() { return "Message"; }
          typedef Events1 Interface;
          typedef std::tuple<std::string, std::string,
                             std::map<std::string, AdkVariant>>
              ArgumentType;
        };
      };
    };
  };

  typedef Service::Interfaces::Events1::Signals::AdkMessage::ArgumentType
      AdkMessageSignalPayload;

  virtual ~Service() = default;
};
}  // namespace ipc
}  // namespace adk

#endif  // INCLUDE_ADK_IPC_SERVICE_H_
