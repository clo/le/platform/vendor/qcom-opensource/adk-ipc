/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef INCLUDE_ADK_IPC_SKELETON_H_
#define INCLUDE_ADK_IPC_SKELETON_H_

#include <core/dbus/bus.h>
#include <core/dbus/service.h>
#include <core/dbus/visibility.h>

namespace adk {
namespace ipc {
/**
 * @brief Skeleton is a template class that helps with exposing interface
 * implementations on the bus. This is the adk version of the Skeleton
 * template based on dbus-cpp version one but allowing replacement of the
 * bus name.
 * @tparam T The type of the interface for which we want to expose an
 * implementation for.
 */
template <typename T>
class ORG_FREEDESKTOP_DBUS_DLL_PUBLIC Skeleton : public T {
 public:
  virtual ~Skeleton() noexcept = default;

 protected:
  /**
   * @brief Skeleton announces the service on the given bus instance.
   * @param bus The bus that the actual service lives upon.
   */
  inline explicit Skeleton(const core::dbus::Bus::Ptr& bus)
      : bus_(bus),
        service_(core::dbus::Service::add_service<T>(
            bus, core::dbus::Bus::RequestNameFlag::do_not_queue |
                     core::dbus::Bus::RequestNameFlag::replace_existing |
                     core::dbus::Bus::RequestNameFlag::allow_replacement)) {}

  /**
   * @brief access_bus provides access to the underlying bus instance.
   * @return A reference to the underlying bus.
   */
  inline const core::dbus::Bus::Ptr& access_bus() const { return bus_; }

  /**
   * @brief access_service provides access to the underlying service object that
   * this object is
   * a proxy for.
   * @return A reference to the underlying service object.
   */
  inline const core::dbus::Service::Ptr& access_service() const {
    return service_;
  }

 private:
  core::dbus::Bus::Ptr bus_;
  core::dbus::Service::Ptr service_;
};
}  // namespace ipc
}  // namespace adk

#endif  // INCLUDE_ADK_IPC_SKELETON_H_
