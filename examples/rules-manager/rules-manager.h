/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef EXAMPLES_RULES_MANAGER_RULES_MANAGER_H_
#define EXAMPLES_RULES_MANAGER_RULES_MANAGER_H_

#include <memory>

#include "msgqueue.h"
#include "adk/ipc/serviceemitter.h"
#include "adk/ipc/servicelistener.h"

namespace adk {
namespace examples {
/**
 * @brief This is a simple example of a adk-ipc service that acts both as a
 * server and client.
 *
 * Its server side is implemented following these two steps:
 * Step1: ADK server API implementation
 * It implements a derived class from the base adk-ipc Emitter
 * class, passing its module's name to its base class constructor to be used
 * as this service's D-BUS connection/bus name and providing a Ptr typedef to
 * its own shared_ptr.
 * It implements the StartServerService API where this server's main loop and
 * internal processing is initialized. The service emits ADK signals by
 * calling the public adk-ipc API methods EmitMessageSignal or
 * EmitStatusSignal
 *
 * Step2: Private Detail implementation
 * It implements its internal processing privately. This simple example service
 * server implements a basic hardcoded status to Message rule converter and runs
 * in a loop printing and emitting adk Message signals when there are adk
 * statuses to be processed in its internal adk event queue.
 *
 *
 * Its client side is implemented following these two steps:
 * Step1: ADK client API implementation
 * It implements a derived class from the base adk-ipc Listener class,
 * passing its module's name to its base class constructor to be used as this
 * service's D-BUS connection/bus name and providing a Ptr typedef to its
 * own shared_ptr.
 * It implements the SubscribeSignalListener API where it subscribes signal
 * callback handlers to listen for ADK signals matching specified set  of
 * arguments. Subscription is implemented by calling the public adk-ipc
 * API methods ListenMessageSignalWithArgs or ListenStatusSignalWithArgs.
 *
 * Step2: Private Detail implementation
 * It implements its internal processing privately within the registered
 * callback
 * handlers. This simple example service client only prints out the signals
 * received by its status signal handler and queues these statuses signals
 * in its internal adk event queue.
 *
 *
 * Lastly, the last step is Public instantiation and deployment:
 * This service is instantiated from the main() application by calling the
 * adk-ipc service announcer utility resolve_service_on_bus_with_listener().
 * The ADK IPC service for each them, client and the server services are
 * then run each on its own thread by calling the public API
 * LaunchAdkConnection() on both the client and server side.
 *
 * */
class RulesManagerService final : public ipc::Emitter, public ipc::Listener {
 public:
  // Needed by announce_service_on_bus_with_listener()
  typedef std::shared_ptr<RulesManagerService> Ptr;

 public:
  RulesManagerService();

  void StopServerService();

 private:
  // From Emitter
  bool StartServerService() override;
  // From Listener
  bool StartSignalListeners() override;

  void WorkerListenerThread();

 private:
  struct Private;
  std::unique_ptr<Private> priv_;
};
}  // namespace examples
}  // namespace adk

#endif  // EXAMPLES_RULES_MANAGER_RULES_MANAGER_H_
