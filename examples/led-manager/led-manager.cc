/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "led-manager.h"

#include <core/posix/signal.h>
#include <string>

#include "adk/ipc/servicefactory.h"

namespace adk {
namespace examples {
bool LedMgrService::StartSignalListeners() {
  // Here register handlers for the signals and matching arguments of interest
  // by defining signal argument pairs to match within the signal and calling
  // the ListenXXXSignalWithArgs methods from the base Listener class.

  // This first listener listens to Led Message signals
  // Match the namespace of the listened signal to Led (i.e arg0="Led")
  DBusMatchRuleArgs match_arg_command{{0, "Led"}};
  ListenMessageSignalWithArgs(
      [](AdkMessageSignalPayload command) {
        // Here is where the internals of the led-manager event handler will do
        // some stuff with the command received.

        // arg2 within payload is expected to be a map of key-variant pairs
        // In this example the keys expected are: "group" and "pattern"
        auto dict = std::get<2>(command);
        if (dict.find("group") == dict.end() ||
            dict.find("pattern") == dict.end()) {
          std::cout << "This was not the expected signal." << std::endl;
        } else {
          std::cout
              << "Handler1> AdkMessage signal has been received with "
                 "argument: "
              << std::get<0>(command) << " " << std::get<1>(command) << " "
              // The variant types for this command signal are known to be
              // a std::string and std::uint32_t and can be decoded as such.
              // Any attempt to decode these values to any different type would
              // incur a (recoverable) runtime error.
              << "group: " << dict.at("group").as<std::string>() << " "
              << "pattern: " << dict.at("pattern").as<std::uint32_t>()
              << std::endl;
        }
      },
      match_arg_command);

  // Register to listen directly to Button Status signal
  // This is an example of Listening to a signal from a different emitter
  // to the one registered above
  DBusMatchRuleArgs match_arg_status{{0, "Button"}};
    ListenMessageSignalWithArgs(
          [](const AdkMessageSignalPayload& status) {
        // Here is where the internals of the led-manager event handler will do
        // some stuff with the status received.

        // arg2 within payload is expected to be a map of variants
        // Check whether this is the expected status signal and
        // if it is decode the key value pairs.
        auto dict = std::get<2>(status);
        if (dict.find("buttonid") == dict.end() ||
            ((std::get<1>(status) == "pressed") &&
             dict.find("duration") == dict.end())) {
          std::cout << "This was not the expected signal." << std::endl;
        } else {
          std::cout
              << "Handler2> AdkStatus signal has been received with argument: "
              << std::get<0>(status) << " " << std::get<1>(status) << " ";

          for (const auto& key : dict) {
            std::cout << key.first << " "
                      // The variant type for this status signal is known
                      // to be a std::string and can be decoded as such.
                      // Any attempt to decode it to a different type would
                      // incur a (recoverable) runtime error.
                      << dict.at(key.first).as<std::string>() << std::endl;
          }
        }
      },
      match_arg_status);
  return true;
}
}  // namespace examples
}  // namespace adk

int main(int, char**) {
  // Instantiate a SignalTrap object to handle blocking of the enumerated
  // list of system termination signals we wish to block. The signaltrap
  // object will allow this program to exit gracefully when intercepting
  // those signals.
  auto signaltrap = core::posix::trap_signals_for_process(
      {core::posix::Signal::sig_int, core::posix::Signal::sig_term});

  // Register a callback function with the signaltrap object to be notified
  // whenever one of the blocked signals raised by the operating system
  // is trapped. The callback function will exit the trap listening loop
  // so that this program can perform any required cleanup of the running
  // service before terminating.
  signaltrap->signal_raised().connect(
      [signaltrap](core::posix::Signal) { signaltrap->stop(); });

  // Instantiate a LedMgrService (dbus-cpp service to access D-Bus IPC).
  auto stub =
      adk::ipc::resolve_service_on_bus<adk::ipc::Service,
                                        adk::examples::LedMgrService>();

  // Launch a ADK connection for this service
  if (stub->LaunchConnection() == false) {
    return EXIT_FAILURE;
  }
  // Start listening for system incoming signals and block program
  // until signaltrap->stop() is called.
  signaltrap->run();

  std::cout << "led-manager (example) exiting..." << std::endl;

  return EXIT_SUCCESS;
}
