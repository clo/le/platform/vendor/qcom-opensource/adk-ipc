/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "rules-manager.h"

#include <core/posix/signal.h>
#include <unistd.h>
#include <iostream>
#include <map>
#include <string>
#include <tuple>

#include "adk/ipc/servicefactory.h"

namespace adk {
namespace examples {
struct RulesManagerService::Private {
  // Internal qsap event queue
    adk::MsgQueue<AdkMessageSignalPayload> m_eventQ;
  bool shutting_down = {false};
};

RulesManagerService::RulesManagerService()
    : Emitter("com.qualcomm.qti.adk1.rules-manager.server.example"),
      Listener("com.qualcomm.qti.adk1.rules-manager.client.example"),
      priv_(new Private()) {}

bool RulesManagerService::StartServerService() {
  std::cout << "RulesManager::StartServerService()" << std::endl;

  auto eventlistener =
      std::move(std::thread{[this]() { WorkerListenerThread(); }});

  eventlistener.detach();
  return true;
}

void RulesManagerService::WorkerListenerThread() {
  std::cout << "RulesManager::WorkerListenerThread()" << std::endl;

  // Here is where the private implementation detail of this module goes.

  // Create signals to emit
  std::map<std::string, AdkVariant> map;

  // Note: Once the qsap payload values (i.e. variant dbus values in the
  // key-value pairs within the map) are encoded within the qsap signal map,
  // the dbus signal message is then ready to be emitted on the bus and it
  // should not be accessed or extracted from the map in anyway as otherwise
  // the dbus message may be corrupted.
  map["pattern"] = AdkVariant::encode<std::uint32_t>(1);
  map["group"] = AdkVariant::encode<std::string>("wifi");
  auto arg_lon = std::make_tuple("Led", "turn_on", map);
  auto arg_loff = std::make_tuple("Led", "turn_off", map);

  while (!priv_->shutting_down) {
    // Process any signals arriving to the event Q
    auto event = priv_->m_eventQ.remove();

    if (std::get<1>(event).compare("pressed") == 0) {
      if (!EmitMessageSignal(arg_lon)) {
        // There has been an error while emitting this ADK signal.
        // For this example we just print out there has been an error.
        std::cout << "Error while emitting this RulesManager Message signal"
                  << std::endl;
      } else {
        std::cout
            << "RulesManager -> emitted AdkMessage signal with arguments "
            << std::get<0>(arg_lon) << " " << std::get<1>(arg_lon) << " "
            << std::endl;
      }
    } else if (std::get<1>(event).compare("released") == 0) {
      if (!EmitMessageSignal(arg_loff)) {
        // There has been an error while emitting this ADK signal.
        // For this example we just print out there has been an error.
        std::cout << "Error while emitting this RulesManager Message signal"
                  << std::endl;
      } else {
        std::cout
            << "RulesManager -> emitted AdkMessage signal with arguments "
            << std::get<0>(arg_loff) << " " << std::get<1>(arg_loff) << " "
            << std::endl;
      }
    }
  }
}

bool RulesManagerService::StartSignalListeners() {
  std::cout << "RulesManager::StartSignalListeners()" << std::endl;

  // Define signal argument pairs to match
  DBusMatchRuleArgs match_button_arg0{{0, "Button"}};

  // Register handler for the signals and matching arguments of interest.
    ListenMessageSignalWithArgs(
      // [this](AdkStatusSignalPayload status) {
        [this](AdkMessageSignalPayload status) {
        // Here is where the internal implementation of the RulesManager event
        // handler may do some stuff. Currently just queue the event for
        // processing by the WorkerListenerThread.

        if (!priv_->shutting_down) {
          // arg2 within the payload of this particular status signal is
          // expected to be a map with a single key-value pair with the value
          // being a variant of type std::string.
          auto dict = std::get<2>(status);
          std::cout
              << "Handle1: AdkStatus signal has been received with args: "
              << std::get<0>(status) << " " << std::get<1>(status) << " ";
          for (const auto& key : dict) {
            std::cout << key.first << " "
                      // the variant type for this status signal is known
                      // to be a std::string
                      << dict.at(key.first).as<std::string>() << std::endl;
          }
          priv_->m_eventQ.add(status);
        }
      },
      match_button_arg0);

  return true;
}

void RulesManagerService::StopServerService() {
  std::cout << "StopServerService()" << std::endl;

  // Implementation specific: Shut down any running service
  // and perform any cleaning as needed.
  priv_->shutting_down = true;
}
}  // namespace examples
}  // namespace adk

int main() {
  // Instantiate a SignalTrap object to handle blocking of the enumerated
  // list of system termination signals we wish to block. The signaltrap
  // object will allow this program to exit gracefully when intercepting
  // those signals.
  auto signaltrap = core::posix::trap_signals_for_all_subsequent_threads(
      {core::posix::Signal::sig_int, core::posix::Signal::sig_term});

  // Register a callback function with the signaltrap object to be notified
  // whenever one of the blocked signals raised by the operating system
  // is trapped. The callback function will exit the trap listening loop
  // so that this program can perform any required cleanup of the running
  // service before terminating.
  signaltrap->signal_raised().connect(
      [signaltrap](core::posix::Signal) { signaltrap->stop(); });

  int er = EXIT_SUCCESS;
  // Instantiate a RulesManagerService (dbus-cpp service to access D-Bus IPC).
  auto rulesmanager = adk::ipc::announce_service_on_bus_with_listener<
      adk::ipc::Service, adk::examples::RulesManagerService>();

  // Run server DBus emitter on its own thread
  auto serverthread = std::move(std::thread{[rulesmanager, signaltrap, &er]() {
    // Launch a ADK IPC connection for the server side
    if (rulesmanager->adk::ipc::Emitter::LaunchConnection() == false) {
      er = EXIT_FAILURE;
      signaltrap->stop();
    }
  }});

  // Run client DBus listener on its own thread
  auto clientthread = std::move(std::thread{[rulesmanager, signaltrap, &er]() {
    // Launch a ADK IPC connection for the client side
    if (rulesmanager->adk::ipc::Listener::LaunchConnection() == false) {
      er = EXIT_FAILURE;
      signaltrap->stop();
    }
  }});

  // Start listening for system incoming signals and block program
  // until signaltrap->stop() is called.
  signaltrap->run();

  // Stop execution of the rulesmanager service.
  rulesmanager->StopServerService();

  std::cout << "rulesmanager (example) exiting ... with error code: " << er
            << std::endl;

  if (clientthread.joinable()) clientthread.join();
  if (serverthread.joinable()) serverthread.join();

  return er;
}
