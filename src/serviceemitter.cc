/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "adk/ipc/serviceemitter.h"

#include <iostream>

#include "adk/ipc/session-bus.h"
#include "adk/ipc/traits.h"

namespace dbus = core::dbus;

namespace adk {
namespace ipc {
Emitter::Emitter(const dbus::Bus::Ptr& bus, const std::string& conname)
    : Skeleton<Service>(bus),
      connectionname_(conname),
      object_(access_service()->add_object_for_path(dbus::types::ObjectPath(
          dbus::traits::Service<Service>::object_path()))) {}

Emitter::Emitter(const std::string& conname)
    : Emitter(the_session_bus(), conname) {}

bool Emitter::LaunchConnection() {
  std::cout << "Emitter::LaunchADKConnection()" << std::endl;
  try {
    // Request a connection name explicitly for this module
    std::cout << "request_name_on_bus " << connectionname_ << std::endl;
    access_bus()->request_name_on_bus(
        connectionname_, dbus::Bus::RequestNameFlag::do_not_queue |
                             dbus::Bus::RequestNameFlag::replace_existing);
  } catch (const std::runtime_error& e) {
    std::cout << e.what() << std::endl;
    return false;
  }
  return (StartServerService());
}

bool Emitter::EmitMessageSignal(const AdkMessageSignalPayload& payload) const {
  try {
    object_->emit_signal<
        Service::Interfaces::Events1::Signals::AdkMessage,
        Service::Interfaces::Events1::Signals::AdkMessage::ArgumentType>(
        payload);
  } catch (const std::runtime_error& e) {
    std::cout << e.what() << std::endl;
    return false;
  }
  return true;
}

}  // namespace ipc
}  // namespace adk
