ADK IPC Library
===============

This is the underlying ADK IPC service class.

It is expected that applications will make use of the adk-message-service library
rather than interfacing directly with this ADK IPC library.

Requirements
------------

* Git
* CMake
* A C++ compiler, e.g., GCC, 

How to build
-----------
apt-get install libdbus-glib-1-dev

```
mkdir build
cd build
cmake ../
make
```

The ADK IPC library comes with some example modules that are not built by default.
To build the modules set the WITH_IPC_EXAMPLES flag on, i.e.:

```
cmake .. -DWITH_IPC_EXAMPLES=ON
```
