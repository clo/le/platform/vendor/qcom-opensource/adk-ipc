/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "adk/ipc/servicelistener.h"

#include <iostream>

#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include "adk/ipc/session-bus.h"
#include "adk/ipc/traits.h"

namespace dbus = core::dbus;

namespace adk {
namespace ipc {
Listener::Listener(const dbus::Bus::Ptr& bus, const std::string& conname)
    : dbus::Stub<Service>(bus), connectionname_(conname) {
  auto object = access_service()->object_for_path(
      dbus::types::ObjectPath(dbus::traits::Service<Service>::object_path()));
  commandsignal_ = object->get_signal<
      Service::Interfaces::Events1::Signals::AdkMessage>();  // NOLINT
}

Listener::Listener(const std::string& conname)
    : Listener(the_session_bus(), conname) {}

Listener::~Listener() {
  access_bus()->stop();
  if (workerthread_.joinable()) {
    workerthread_.join();
  }
}

bool Listener::LaunchConnection() {
  std::cout << "Listener::LaunchConnection()" << std::endl;

  auto bus = access_bus();

  try {
    // Request a connection name explicitly for this module.
    std::cout << "request_name_on_bus " << connectionname_ << std::endl;
    bus->request_name_on_bus(connectionname_,
                             core::dbus::Bus::RequestNameFlag::not_set);
  } catch (const std::runtime_error& e) {
    std::cout << e.what() << std::endl;
    return false;
  }

  // Create a generic match rule to listen to all events emitted by a
  // QTI_ADK_PATH object, independent of sender.
  dbus::MatchRule rule;
  rule.type(dbus::Message::Type::signal)
      .sender("")
      .interface("")
      .member("")
      .path(dbus::types::ObjectPath(
          dbus::traits::Service<Service>::object_path()));
  // Add this rule to the bus
  // (calls the low level DBus api with this matching rule).
  bus->add_match(rule);

  // Create a new thread to listen on this bus
  workerthread_ = std::move(std::thread{[bus]() { bus->run(); }});

  return (StartSignalListeners());
}
}  // namespace ipc
}  // namespace adk
