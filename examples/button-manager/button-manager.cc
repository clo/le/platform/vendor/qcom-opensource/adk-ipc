/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "button-manager.h"

#include <core/posix/signal.h>
#include <unistd.h>
#include <map>
#include <string>
#include <tuple>

#include "adk/ipc/servicefactory.h"

namespace adk {
namespace examples {
bool ButtonMgrService::StartServerService() {
  auto buttonlistener = std::move(std::thread{[this]() {
    // Create signals to emit
    // Note: Once the qsap payload values (i.e. variant dbus values in the
    // key-value pairs within the map) are encoded within the qsap signal map,
    // the dbus signal message is then ready to be emitted on the bus and it
    // shouldnot be accessed or extracted from the map in anyway as otherwise
    // the dbus message may be corrupted.
    std::map<std::string, AdkVariant> map1;
    std::map<std::string, AdkVariant> map2;

    // button released status signal contains one single key-value pair
    // with key buttonid
    map1["buttonid"] = AdkVariant::encode<std::string>("button_id_1");
    auto arg_br = std::make_tuple("Button", "released", map1);

    // button pressed status signal contains two single key-value pairs
    // with keys: buttonid and duration
    map2["buttonid"] = AdkVariant::encode<std::string>("button_id_1");
    map2["duration"] = AdkVariant::encode<std::string>("long");
    auto arg_bp = std::make_tuple("Button", "pressed", map2);

    // Here is where the private implementation of this manager goes.
    // This dummy service runs in a loop emitting the two status signals
    // every 2 seconds
    while (!shutting_down_) {
      sleep(2);
        if (!EmitMessageSignal(arg_bp)) {
        // There has been an error while emitting this ADK signal.
        // For this example we just print out there has been an error.
        std::cout << "Error while emitting this ButtonMgr status signal"
                  << std::endl;
      } else {
        // Note that the map payload cannot be printed out once encoded in the
        // variant and emitted as it is emptied from the map
        std::cout
            << "ButtonManager -> emitted AdkStatus signal with arguments "
            << std::get<0>(arg_bp) << " " << std::get<1>(arg_bp) << " "
            << std::endl;
      }

      sleep(2);
        if (!EmitMessageSignal(arg_bp)) {
        // There has been an error while emitting this ADK signal.
        // For this example we just print out there has been an error.
        std::cout << "Error while emitting this ButtonMgr status signal"
                  << std::endl;
      } else {
        // Note that the map payload cannot be printed out once encoded in the
        // variant and emitted as it is emptied from the map
        std::cout
            << "ButtonManager -> emitted AdkStatus signal with arguments "
            << std::get<0>(arg_br) << " " << std::get<1>(arg_br) << " "
            << std::endl;
      }
    }
  }});

  buttonlistener.detach();

  return true;
}

void ButtonMgrService::StopServerService() {
  std::cout << "StopServerService()" << std::endl;

  // Implementation specific: Shut down any running service
  // and perform any cleaning as needed.
  shutting_down_ = true;
}
}  // namespace examples
}  // namespace adk

int main(int, char**) {
  // Instantiate a SignalTrap object to handle blocking of the enumerated
  // list of system termination signals we wish to block. The signaltrap
  // object will allow this program to exit gracefully when intercepting
  // those signals.
  auto signaltrap = core::posix::trap_signals_for_process(
      {core::posix::Signal::sig_int, core::posix::Signal::sig_term});

  // Register a callback function with the signaltrap object to be notified
  // whenever one of the blocked signals raised by the operating system
  // is trapped. The callback function will exit the trap listening loop
  // so that this program can perform any required cleanup of the running
  // service before terminating.
  signaltrap->signal_raised().connect(
      [signaltrap](core::posix::Signal) { signaltrap->stop(); });

  // Instantiate a ButtonMgrService (dbus-cpp service to access D-Bus IPC).
  auto service =
      adk::ipc::announce_service_on_bus<adk::ipc::Service,
                                         adk::examples::ButtonMgrService>();

  // Launch a ADK IPC connection for this service
  if (service->LaunchConnection() == false) {
    service->StopServerService();
    return EXIT_FAILURE;
  }

  // Start listening for system incoming signals and block program
  // until signaltrap->stop() is called.
  signaltrap->run();

  // Stop execution of server service.
  service->StopServerService();

  std::cout << "button-manager (example) exiting..." << std::endl;
  return EXIT_SUCCESS;
}
