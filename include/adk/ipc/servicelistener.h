/*
Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef INCLUDE_ADK_IPC_SERVICELISTENER_H_
#define INCLUDE_ADK_IPC_SERVICELISTENER_H_

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include <core/dbus/signal.h>
#include <core/dbus/stub.h>
#include "adk/ipc/service.h"

namespace adk {
namespace ipc {
/**
 * @brief Listener is an abstract specialized class of the dbus-cpp
 * Stub template class to proxy "/com/qualcomm/qti/adk1" D-Bus objects on the bus.
 * It hides the dbus-cpp layer from its derived classes, allowing connection
 * to the session bus and subscription of Message and Status signals
 * listeners on the bus.
 */
class Listener : public core::dbus::Stub<Service> {
 public:
  typedef std::vector<std::pair<std::size_t, std::string>> DBusMatchRuleArgs;

  typedef core::dbus::Signal<
      Service::Interfaces::Events1::Signals::AdkMessage,
      Service::Interfaces::Events1::Signals::AdkMessage::ArgumentType>
      DBusAdkMessageSignal;

  ~Listener() override;

  /**
    * @brief LaunchConnection should be called after the specialized derived
    * stub instantiation to properly initialize the connection to the ADK bus.
    * It will explicitly request a dbus connection name for this module and it
    * will generate a call to the derived stub implementation of
    * SubscribeSignalListeners().
    * @return false if an error condition prevents the service from being
    * successfully launched, true otherwise.
    */
  virtual bool LaunchConnection();

  /**
   * @brief ListenMessageSignalWithArgs allows clients to subscribe listeners
   * to specific Message signals which payloads match the specified arguments
   * @param h The handler to be invoked when the signal is emitted.
   * @param match_args The matching arguments as a vector of <size_t, string>
   * pairs.
   * @return Subscription token.
   */

  virtual DBusAdkMessageSignal::SubscriptionToken ListenMessageSignalWithArgs(
      const DBusAdkMessageSignal::Handler& h,
      const DBusMatchRuleArgs& match_args) const {
    return (commandsignal_->connect_with_match_args(h, match_args));
  }

  /**
   * @brief UnregisterMessageListener allow clients to unregister listeners.
   * @param token The SubscriptionToken for MessageSignal listeners.
   */
  virtual void UnregisterMessageListener(
      const DBusAdkMessageSignal::SubscriptionToken token) {
    commandsignal_->disconnect(token);
  }

 protected:
  /**
   * @brief Constructs an instance of Listener and connects to the bus
   * with the specified bus/connection name.
   * @param conname The bus/connection name used by this module.
   */
  explicit Listener(const std::string& conname);

  /**
   * @brief Constructs an instance of Listener and connects to the bus
   * with the specified bus/connection name.
   * @param bus The instance of the bus this module is connecting to.
   * @param conname The bus/connection name used by this module.
   */
  Listener(const core::dbus::Bus::Ptr& bus, const std::string& conname);

  // Explicitly disable copy and assignment operators
  Listener(const Listener&) = delete;
  Listener& operator=(const Listener&) = delete;

  /**
    * @brief StartSignalListeners must be implemented by derived classes.
    * It will be called after dbus initialization has been completed to allow
    * initalization of the derived service. The specialized ServiceStub can
    * listen to ADK IPC signals that match particular set of arguments by calling
    * the ListenMessageSignalWithArgs() and ListenStatusSignalWithArgs() public
    * API methods passing a callback handler.
    * The implementation of this method should not block.
    * @return false if an error condition prevents this service from being
    * ready to start listening, true otherwise.
    */
  virtual bool StartSignalListeners() = 0;

 private:
  std::shared_ptr<DBusAdkMessageSignal> commandsignal_;
  const std::string connectionname_;
  std::thread workerthread_;
};
}  // namespace ipc
}  // namespace adk
#endif  // INCLUDE_ADK_IPC_SERVICELISTENER_H_

